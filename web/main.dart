import 'dart:html' as html;
import 'dart:async';

import 'package:logging/logging.dart';
import 'package:console_log_handler/console_log_handler.dart';

import 'package:route_hierarchical/client.dart';

import 'package:mdl/mdl.dart';

import 'package:car_dash/client/components.dart';
import 'package:car_dash/client/stores.dart';
import 'package:car_dash/client/controllers.dart';

final Logger _logger = new Logger('layout-header-drawer-footer');

main() async {

    configLogging();

    // Init MDL
    registerMdl();
    registerAllComponents();

    final Application application = await componentFactory()
        .rootContext(Application)
        .addModule(new StoreModule())
        .run();

    // Config routes

    RoutePreEnterEventHandler loginChecker = (final RoutePreEnterEvent event) {
        event.allowEnter(new Future<bool>(() => true/*application.isUserLoggedIn*/));
    };

    final ViewFactory view = new ViewFactory();

    Router router = application.router;
    router.root

//        ..addRoute(name: 'view1', path: '/view1',
//            enter: view("views/view1.html", new ControllerView1()),
//            preEnter: loginChecker)
//
//        ..addRoute(name: 'view2', path: '/view2',
//            enter: view("views/view2.html", new ControllerView2()),
//            preEnter: loginChecker)
//
        ..addRoute(name: 'Vehicles', path: '/myvehicles',
            enter: view("views/list.html", new DefaultController()),
            preEnter: loginChecker)

        ..addRoute(name: 'Collectors', path: '/collectors',
            enter: view("views/collector-list.html", new DefaultController()),
            preEnter: loginChecker)

        ..addRoute(name: 'Home2', path: '/home2',
            enter: view("views/home2.html", new DefaultController()),
            preEnter: loginChecker)

        // Leave default-route as the last one
        ..addRoute(name: 'Home', defaultRoute: true, path: '/',
            enter: view("views/home.html", new DefaultController()))

    ;

    router.listen();

    application.run();
}

void configLogging() {
    hierarchicalLoggingEnabled = false; // set this to true - its part of Logging SDK

    // now control the logging.
    // Turn off all logging first
    Logger.root.level = Level.INFO;
    Logger.root.onRecord.listen(new LogConsoleHandler());
}

