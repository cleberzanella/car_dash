part of car_dash.client.components;

@MdlComponentModel @di.Injectable()
class Application extends MaterialApplication {
  final Logger _logger = new Logger('main.Application');
  final Router _router = new Router(useFragment: true);

  /// Title will be displayed
  final ObservableProperty<String> title = new ObservableProperty<String>("");

  bool isUserLoggedIn = false;

  get router => _router;

  Application() {
    _logger.info("Application created");
    _bindSignals();
  }

  @override
  void run() {
    _login().then((final MdlDialogStatus status) {
      _checkStatus(status);
    });
  }

  void go(final String routePath, final Map params) {
    _router.go(routePath,params);
  }

  void _bindSignals() {
    final MaterialButton login = MaterialButton.widget(dom.querySelector("#login"));
    login.onClick.listen( (final dom.Event event) async {
      event.preventDefault();

      final MdlDialogStatus status = await _login();
      _checkStatus(status);
    });
  }

  Future<MdlDialogStatus> _login() async {
    final LoginDialog dialog = new LoginDialog();
    final MdlDialogStatus status = await dialog(title: "Login").show();

    if(status == MdlDialogStatus.OK) {
      _logger.info("You entered - username: ${dialog.username.value}, password: ${dialog.password.value}");
    }

    return status;
  }

  void _checkStatus(final MdlDialogStatus status) {
    _logger.info("Status: ${status}");
    isUserLoggedIn = (status == MdlDialogStatus.OK);

    if(isUserLoggedIn) {
      go("view1", {} );
    } else {
      go("home", {} );
    }
  }
}
