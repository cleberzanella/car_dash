/*
 * Copyright (c) 2016, Michael Mitterer (office@mikemitterer.at),
 * IT-Consulting and Development Limited.
 *
 * All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

part of car_dash.client.components;

/// Controller for <name-editor></name-editor>
///
@MdlComponentModel
class CollectorViewComponent extends MdlTemplateComponent {
  final Logger _logger = new Logger('car_dash.client.components.CollectorViewComponent');

  //static const _CollectorViewComponentConstant _constant = const _CollectorViewComponentConstant();
  static const _CollectorViewComponentCssClasses _cssClasses = const _CollectorViewComponentCssClasses();

  /// Only a single person is necessary
  final CollectorStore _store;

  CollectorViewComponent.fromElement(final dom.HtmlElement element,final di.Injector injector)
      : super(element,injector), _store = injector.get(CollectorStore) {

    _init();
  }

  static CollectorViewComponent widget(final dom.HtmlElement element) => mdlComponent(element,CollectorViewComponent) as CollectorViewComponent;

  String get id => element.dataset["id"];

  String get time => _store.time;

  String get deviceTypeName => _store.byId(id).typeName;
  String get deviceOsName => _store.byId(id).osName;
  String get deviceNickname => _store.byId(id).nickname;

  String get firstCharacter {
    final String nickname = deviceNickname;
    return nickname.isNotEmpty ? nickname.substring(0,1).toUpperCase() : "";
  }

  String get iconcolor => _colorName(firstCharacter);


  void _init() {
    _logger.fine("CollectorViewComponent - init");

    element.classes.add(_CollectorViewComponentConstant.WIDGET_SELECTOR);
    element.classes.add("sample-inplace-edit"); // Gambiarra pra não precisar reescrever o SCSS

    render().then((_) {
      _bindActions();
    });

    element.classes.add(_cssClasses.IS_UPGRADED);
  }

  /// After the template is rendered we bind all the necessary events for this component
  void _bindActions() {
    _store.onChange.listen((final DataStoreChangedEvent event) {
      // This message comes every second - so we optimize this update section a bit
      if(event.data.actionname == UpdateTimeView.NAME) {
        _updateTime();
      }
    });
  }


  void _updateTime() {
    element.querySelector(".${_cssClasses.TIME} > span").text = _store.time;
  }

  String _colorName(final String character) {
    final String firstCharacter = character.isNotEmpty ? character.substring(0,1).toLowerCase() : "";

    switch(firstCharacter.toLowerCase()) {
      case 'a':
      case 't':
        return 'mdl-color--red';
      case 'b':
      case 'u':
        return 'mdl-color--pink';
      case 'c':
      case 'v':
        return 'mdl-color--purple';
      case 'd':
      case 'w':
        return 'mdl-color--deep-purple';
      case 'e':
      case 'x':
        return 'mdl-color--indigo';
      case 'f':
      case 'y':
        return 'mdl-color--blue';
      case 'g':
      case 'z':
        return 'mdl-color--light-blue';
      case 'g':
        return 'mdl-color--cyan';
      case 'i':
        return 'mdl-color--teal';
      case 'j':
        return 'mdl-color--green';
      case 'k':
        return 'mdl-color--light-green';
      case 'l':
        return 'mdl-color--lime';
      case 'm':
      case 'n':
        return 'mdl-color--amber';
      case 'o':
        return 'mdl-color--orange';
      case 'p':
        return 'mdl-color--deep-orange';
      case 'q':
        return 'mdl-color--brown';
      case 'r':
        return 'mdl-color--grey';
      case 's':
        return 'mdl-color--blue-grey';
      default: return 'mdl-color--red';
    }

  }

  @override
  String get template => """
        <div class="sample-inplace-edit__container">
            <div class="sample-inplace-edit__title">
                <span class="sample-inplace-edit__title-icon mdl-icon material-icons {{iconcolor}}">{{deviceTypeName}}</span>
                <span class="sample-inplace-edit__title-data">{{deviceOsName}}, {{deviceNickname}}</span>
                <span class="sample-inplace-edit__title-time">Last reading: <span>{{time}}</span></span>
            </div>
        </div>
    """.trim().replaceAll(new RegExp(r"\s+")," ");

}

/// Registers the CollectorViewComponent-Component
///
///     main() {
///         registerCollectorViewComponent();
///         ...
///     }
///
void registerCollectorViewComponent() {
  final MdlConfig config = new MdlWidgetConfig<CollectorViewComponent>(
      _CollectorViewComponentConstant.WIDGET_SELECTOR,
      (final dom.HtmlElement element,final di.Injector injector) => new CollectorViewComponent.fromElement(element,injector)
  );

  // If you want <name-editor></name-editor> set selectorType to SelectorType.TAG.
  // If you want <div name-editor></div> set selectorType to SelectorType.ATTRIBUTE.
  // By default it's used as a class name. (<div class="name-editor"></div>)
  config.selectorType = SelectorType.TAG;

  componentHandler().register(config);
}

class _CollectorViewComponentCssClasses {

  final String IS_UPGRADED = 'is-upgraded';

  final String ACTIVE = 'active';

  final String SHOW_CONTENT = 'show-content';

  final String CONTAINER = 'sample-inplace-edit__container';
  final String TIME = 'sample-inplace-edit__title-time';
  final String TITLE = 'sample-inplace-edit__title-data';

  final String TITLE_ICON = 'sample-inplace-edit__title-icon';
  final String CONTENT_ICON = 'sample-inplace-edit__content-icon';

  const _CollectorViewComponentCssClasses(); }

class _CollectorViewComponentConstant {

  static const String WIDGET_SELECTOR = "collector-view";

  const _CollectorViewComponentConstant();
}