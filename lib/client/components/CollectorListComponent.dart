part of car_dash.client.components;

@MdlComponentModel
class CollectorListComponent extends MdlComponent implements ScopeAware {
  final Logger _logger = new Logger('car_dash.client.components.CollectorListComponent');

  static const _CollectorListComponentCssClasses _cssClasses = const _CollectorListComponentCssClasses();

  /// Holds all the persons for this sample
  final CollectorsStore _store;

  Scope scope;

  CollectorListComponent.fromElement(final dom.HtmlElement element,final di.Injector injector)
      : super(element,injector),_store = injector.get(CollectorsStore) {

    scope = new Scope(this,mdlParentScope(this));
    _init();
  }

  static CollectorListComponent widget(final dom.HtmlElement element) => mdlComponent(element,CollectorListComponent) as CollectorListComponent;

  /// Make persons available for mdl-repeat
  ObservableList<CollectorDevice> get collectors => _store.devices;

  // - EventHandler -----------------------------------------------------------------------------

  void handleButtonClick() {
    _logger.info("Event: handleButtonClick");
  }

  //- private -----------------------------------------------------------------------------------

  void _init() {
    _logger.info("CollectorListComponent - init");

    // Recommended - add SELECTOR as class if this component is a TAG!
    element.classes.add(_CollectorListComponentConstant.WIDGET_SELECTOR);

    element.classes.add(_cssClasses.IS_UPGRADED);
  }
}

/// Registers the CollectorListComponent-Component
///
///     main() {
///         registerCollectorListComponent();
///         ...
///     }
///
void registerCollectorListComponent() {
  final MdlConfig config = new MdlWidgetConfig<CollectorListComponent>(
      _CollectorListComponentConstant.WIDGET_SELECTOR,
      (final dom.HtmlElement element,final di.Injector injector) => new CollectorListComponent.fromElement(element,injector)
  );

  // If you want <sample-inplace-persons></sample-inplace-persons> set selectorType to SelectorType.TAG.
  // If you want <div sample-inplace-persons></div> set selectorType to SelectorType.ATTRIBUTE.
  // By default it's used as a class name. (<div class="sample-inplace-persons"></div>)
  config.selectorType = SelectorType.TAG;

  componentHandler().register(config);
}

//- private Classes ----------------------------------------------------------------------------------------------------

/// Store strings for class names defined by this component that are used in
/// Dart. This allows us to simply change it in one place should we
/// decide to modify at a later date.
class _CollectorListComponentCssClasses {

  final String IS_UPGRADED = 'is-upgraded';

  const _CollectorListComponentCssClasses(); }

/// Store constants in one place so they can be updated easily.
class _CollectorListComponentConstant {

  static const String WIDGET_SELECTOR = "collector-list";

  const _CollectorListComponentConstant();
}