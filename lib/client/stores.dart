/*
 * Copyright (c) 2016, Michael Mitterer (office@mikemitterer.at),
 * IT-Consulting and Development Limited.
 * 
 * All Rights Reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

library car_dash.components.stores;

import 'dart:async';

import 'package:di/di.dart' as di;
import 'package:logging/logging.dart';
import 'package:validate/validate.dart';
import 'package:intl/intl.dart';

import 'package:mdl/mdl.dart';
import 'package:mdl/mdlflux.dart';

import 'package:car_dash/client/model.dart';

part 'components/interfaces/stores.dart';
part 'components/interfaces/actions.dart';
part 'stores/PersonsStoreImpl.dart';
part 'stores/CollectorStoreImpl.dart';

PersonsStore _singletonStore = null;
CollectorsStore _deviceStore = null;

/// Basic DI configuration for the Application-[DataStore]s
///
/// Usage:
///     class MainModule extends di.Module {
///         MainModule() {
///             install(new StoreModule());
///         }
///     }
class StoreModule  extends di.Module {
    StoreModule() {

        bind(PersonsStore,toFactory: _singletonFactory);
        bind(PersonStore,toFactory: _singletonFactory);
        bind(CollectorsStore,toFactory: _singletonCollectorStoreFactory);
        bind(CollectorStore,toFactory: _singletonCollectorStoreFactory);
    }
}

/// Ugly hack because DI does not support "asSingleton" like Guice does!!!!
_singletonFactory() {
    if(_singletonStore == null) {
        _singletonStore = new PersonsStoreImpl(componentFactory().injector.get(ActionBus));
    }
    return _singletonStore;
}

_singletonCollectorStoreFactory() {
    if(_deviceStore == null) {
        _deviceStore = new CollectorsStoreImpl(componentFactory().injector.get(ActionBus));
    }
    return _deviceStore;
}