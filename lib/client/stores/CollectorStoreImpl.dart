part of car_dash.components.stores;

/// Concrete implementation for our stores.
///
/// We could use two separate implementations for each store but as you can see here,
/// for simplicity we use one store for the whole Application
class CollectorsStoreImpl extends Dispatcher implements CollectorsStore, CollectorStore {
  final Logger _logger = new Logger('mdl_inplace_edit_sample.stores.CollectorsStoreImpl');

  final ObservableList<CollectorDevice> _devices = new ObservableList<CollectorDevice>();

  CollectorsStoreImpl(final ActionBus actionbus) : super(actionbus) {
    Validate.notNull(actionbus);

    _logger.info("CollectorsStoreImpl - CTOR");

    _initSampleData();
    _bindSignals();
    _bindActions();
    _initTimer();
  }

  //- public interfaces -------------------------------------------------------------------------

  ObservableList<CollectorDevice> get devices => _devices;

  /// Returns a NEW [Person] object - we don't want
  /// to modify Store-Objects directly!
  CollectorDevice byId(final String uuid) {
    Validate.isUUID(uuid);
    return new CollectorDevice.from(_byId(uuid));
  }

  String get time {
    return new DateFormat.Hms().format(new DateTime.now());
  }

  //- private -----------------------------------------------------------------------------------

  void _bindActions() {
    on(PersonChangedAction.NAME).listen((final CollectorChangedAction action) {
      final CollectorDevice newDevice = action.data;
      final CollectorDevice oldDevice = _byId(newDevice.id);

      oldDevice.update(newDevice);
      emitChange();
    });
  }

  /// Internal [Person] object - can be modified
  CollectorDevice _byId(final String uuid) {
    Validate.isUUID(uuid);
    return _devices.firstWhere((final CollectorDevice device) => device.id == uuid);
  }

  void _bindSignals() {
  }

  void _initSampleData() {
    _devices.add(new CollectorDevice("computer","Linux", "cleber-laptor"));
    _devices.add(new CollectorDevice("smartphone","Android", "Moto G2"));
    _devices.add(new CollectorDevice("smartphone","iOS", "6S"));
    _devices.add(new CollectorDevice("computer","Windows", "bibi-laptor"));

  }

  /// For demonstration purpose - pump update-messages to the
  /// attached stores
  void _initTimer() {
    new Timer.periodic(new Duration(seconds: 1), (_) => emitChange(action: new UpdateTimeView()));
  }
}
