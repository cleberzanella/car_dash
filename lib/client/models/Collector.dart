part of car_dash.client.model;

@MdlComponentModel
class CollectorDevice {

  final String id;

  String typeName; // "computer" ou smartphone // define o ícone
  String osName; // "Linux" ou Android
  String nickname; // "laptop";

  CollectorDevice(this.typeName, this.osName, this.nickname) : id = new Uuid().v1();

  CollectorDevice.from(final CollectorDevice devic) : typeName = devic.typeName, osName = devic.osName,
        nickname = devic.nickname, id = devic.id;

  void update(final CollectorDevice device) {
    Validate.isTrue(id == device.id);
    typeName = device.typeName;
    osName = device.osName;
    nickname = device.nickname;
  }

}
