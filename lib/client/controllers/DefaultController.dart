part of car_dash.client.controllers;

class DefaultController extends MaterialController {
  final Logger _logger = new Logger('main.DefaultController');

  @override
  void loaded(final Route route) {

    final Application app = componentFactory().application;
    app.title.value = route.name;

    _logger.info("DefaultController loaded!");
  }
}